var express = require('express'),
    router = express.Router(),
    path = require('path'),
    Converter = require("csvtojson").Converter;

module.exports = function (app) {
// isAlive
router.get('/', function(req, res) {
    res.json({ message: 'API ok!' });  
});

router.get('/csv-to-json', function(req, res) {
    var converter = new Converter({constructResult:true});
    converter.fromFile(path.resolve("assets/csv/test.csv"),function(err,result){
        if (err) {
            var message = typeof err === String ? err : err.message;
            res.status(500).send(message);
        } else {
            res.json({result: result});
            console.log('Parsed CSV file!');
        }
    }); 
});

router.get('/csv', function(req, res) {
    if (!req.body.fileName) {
        res.status(400).send('Please provide a file name.')
    }
    var file = './assets/csv/' + req.body.fileName;

    var aa = require(file)
});

app.use('/api', router);
};


