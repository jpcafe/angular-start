var express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path');

// create express server
var app = express();
app.use(bodyParser.json());
// serve static folder
app.use(express.static(path.join(__dirname,'../assets')));

// create http for socket.io and socket server
var http = require('http').Server(app);
require('./socket')(http);

// create routes
require('./routes')(app);

// listen on http instead of app, investigate why...
http.listen(8081);

module.exports = app;