# README #

Angular 1.5 startup project.

Technologies:

- Angular 1.5.8
- Angular router 1.5.8
- Babel (es6)
- Webpack (with dev server)
- socket.io

Installation:

- Install node.js: https://nodejs.org/en/download/
- Clone the repo.
- Run 'npm install' to install all the dependencies
- Run 'node index.js' to startup the web server
- Hit localhost:8080

Next steps:
- Use ui-router (angular-ui-router)
- Add karma, mocha, chai for unit testing
- Add gulp for automated tasks