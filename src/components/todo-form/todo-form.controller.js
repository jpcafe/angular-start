import * as TodoFormActions from '../../actions/todo-form-actions';

class TodoFormController {
    constructor ($ngRedux, $scope) {
        let unsubscribe = $ngRedux.connect(
            this.mapStateToThis, 
            TodoFormActions)
            (this);
        $scope.$on('$destroy', unsubscribe);
    }

    mapStateToThis (state) {
        return {
            todo: state.todoForm.todo,
            updated: state.todoForm.updated
        }
    }
}

export default TodoFormController;