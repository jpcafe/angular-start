import template from './todo-form.html';
import controller from './todo-form.controller';

const formComponent = {
    template,
    controller,
    controllerAs: 'vm'
};

export default formComponent; 