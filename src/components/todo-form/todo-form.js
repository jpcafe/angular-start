import angular from 'angular';
import todoFormComponent from './todo-form.component';

const todoFormModule = angular.module('app.components.todo-form', [])
.component('todoForm', todoFormComponent);

export default todoFormModule;