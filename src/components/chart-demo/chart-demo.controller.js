import io from 'socket.io-client';
import * as ChartDemoActions from '../../actions/chart-demo-actions';

class ChartDemoController {
    constructor ($ngRedux, $scope) {
        let unsubscribe = $ngRedux.connect(
            this.mapStateToThis, 
            ChartDemoActions)
            (this);
        $scope.$on('$destroy', unsubscribe);

        this.SOCKET_URL = 'http://localhost:8081';
        this.socket = io(this.SOCKET_URL);
        
        this.socket.on('connect', this.connectCallback.bind(this));
        this.socket.on('chart-data', this.socketReceive);
    }

    getStatus () {
        return this.isConnected? 'Disconnect':'Connect';
    }

    toggleSocket () {
        if(this.isConnected === true) {
            this.socket.disconnect();
            this.socketDisconnect();
            console.log('Disconnected from web socket on ' + this.SOCKET_URL);
        } else {
            this.socket.connect();
        }
    }

    connectCallback () {
        console.log('Connected to web socket on ' + this.SOCKET_URL);
        this.socketConnect();
    }

     mapStateToThis (state) {
        return {
            isConnected: state.chartDemo.isConnected,
            data: state.chartDemo.data
        }
    }
}

export default ChartDemoController;