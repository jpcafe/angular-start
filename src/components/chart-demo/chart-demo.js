import angular from 'angular';
import chartDemoComponent from './chart-demo.component';
import './chart-demo.css';

const chartDemoModule = angular.module('app.components.chart-demo', [])
.component('chartDemo', chartDemoComponent);

export default chartDemoModule;