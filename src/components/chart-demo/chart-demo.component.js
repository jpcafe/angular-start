import template from './chart-demo.html';
import controller from './chart-demo.controller';

const chartDemoComponent = {
    template,
    controller,
    controllerAs: 'vm'
};

export default chartDemoComponent; 