import angular from 'angular';
import angularRoute from 'angular-route';
import createLogger from 'redux-logger';
import { combineReducers } from 'redux';
import ngRedux from 'ng-redux'; 

import todoFormComponent from './components/todo-form/todo-form';
import chartDemoComponent from './components/chart-demo/chart-demo';
import rootReducer from './reducers'; 

const logger = createLogger({  
  level: 'info',
  collapsed: true
});

angular.module('app', [ngRedux,'ngRoute', 'app.components.todo-form', 'app.components.chart-demo'])
.config(($routeProvider) => {
   $routeProvider
    .when("/todo-demo", {
        template : "<todo-form></todo-form>"
    })
    .when("/chart-demo", {
        template : "<chart-demo></chart-demo>"
    })
    .otherwise({
        template : "<h1>None</h1><p>Nothing has been selected</p>"
    });

    console.log('Router ready!');
})
.config(($ngReduxProvider) => {
    $ngReduxProvider.createStoreWith(rootReducer, [logger]);

    console.log('Store ready!');
});

console.log('Started app!');