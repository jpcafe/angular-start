export const socketReceive = (data) => {
    return {
        type: 'SOCKET_RECEIVE',
        payload: data
    }
};

export const socketConnect = () => {
    return {
        type: 'SOCKET_CONNECT'
    }
};

export const socketDisconnect = () => {
    return {
        type: 'SOCKET_DISCONNECT'
    }
};