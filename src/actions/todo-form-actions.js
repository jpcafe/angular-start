export const editTodo = (todo) => {
    return {
        type: 'EDIT_TODO',
        payload: {todo}
    }
};