const initialState = {
    isConnected: false,
    data: 0
};
const todoFormReducer = (state = initialState, action) => {
switch (action.type) {
    case 'SOCKET_RECEIVE':
        return Object.assign({}, state, {data: action.payload});
    case 'SOCKET_CONNECT':
        return Object.assign({}, state, {isConnected: true});
    case 'SOCKET_DISCONNECT':
        return Object.assign({}, state, {isConnected: false});
    default:
        return state;
}
};

export default todoFormReducer;