const initialState = {
    todo: {name: 'Default Name'},
    updated: false
};
const todoFormReducer = (state = initialState, action) => {
switch (action.type) {
    case 'EDIT_TODO':
        return Object.assign({}, state, {todo: action.payload.todo, updated: true});
    default:
        return state;
}
};

export default todoFormReducer;