import { combineReducers } from 'redux';
import todoForm from './todo-form-reducer';
import chartDemo from './chart-demo-reducer';

const rootReducer = combineReducers({
  todoForm,
  chartDemo
});

export default rootReducer;