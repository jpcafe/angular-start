var path = require("path");
module.exports = {
	entry: {
    app: ["./src/main.js"]
 	 },
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/assets/",
    filename: "bundle.js"
  },
  module: {
  	loaders: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel', // 'babel-loader' is also a valid name to reference
      query: {
        presets: ['es2015']
      }
    },
    {
        test: /\.html$/,
        loader: 'raw'
    },
    { test: /\.css$/, loader: "style-loader!css-loader" }
  ]
  },
  resolve: {
  root: path.resolve(__dirname),
  alias: {
    assets: path.resolve(__dirname) + '/assets'
  }
},
  devtool: '#inline-source-map'
};